package com.oF2pks.chairlock;

public interface LaunchCallbacks {

    public void onItemSelected(String packageName, String permPackages);
}
