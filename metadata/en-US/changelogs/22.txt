hotfix (R/30) Treble dynamic linkerconfig
...
# Upgrade to android 11 (R/30) + Export: clipboard/share/save
# Collector:
*Camera colorFilter + fix buggy logicalCam
*Treble ld.config [Vendor]
*System ro.boot section
*kDI self targetedSDK
# packagesInfos: + foreground service, overlays 
# Options: add systemUser + pre-N alternate webView
