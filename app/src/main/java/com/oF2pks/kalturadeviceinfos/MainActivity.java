package com.oF2pks.kalturadeviceinfos;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.UiModeManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.os.StrictMode;
import android.os.UserManager;
import android.provider.Settings;


import android.text.util.Linkify;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.oF2pks.xmlapkparser.AXMLPrinter;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.nio.charset.StandardCharsets;

import static com.oF2pks.kalturadeviceinfos.Utils.getProp;
import static com.oF2pks.kalturadeviceinfos.Utils.getZinfo;
import static com.oF2pks.kalturadeviceinfos.Utils.manifestZinfo2;


public class MainActivity extends Activity implements SearchView.OnQueryTextListener {

    private static String report;
    private Intent viewManifestIntent;
    private File output;

    private void showReport(String report) {
        TextView reportView = (TextView) findViewById(R.id.textView);
        assert reportView != null;
        reportView.setText(report);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        showActionsDialog();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        showActionsDialog();
        return false;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Context ctx = this;
        boolean titled = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_main);
        if(titled){
            getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.kdi_bar);
        }
        if(BuildConfig.DEBUG) {
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .build());
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().build());
        }
        StrictMode.allowThreadDiskReads();
        StrictMode.allowThreadDiskWrites();

        // Collect data
        new CollectorTask(MainActivity.this, getBaseContext()).execute(false);

        Button bShare = findViewById(R.id.export_button);
        assert bShare != null;
        bShare.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                showExportDialog();
            }
        });

        Button bRefresh = findViewById(R.id.refresh_button);
        assert bRefresh != null;
        bRefresh.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                setContentView(R.layout.content_main);
                new CollectorTask(MainActivity.this, getBaseContext()).execute(false);
            }
        });

        Button bOptions = findViewById(R.id.options_button);
        assert bOptions != null;
        bOptions.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                showActionsDialog();
            }
        });

        Button bEnd = findViewById(R.id.exit_button);
        assert bEnd != null;
        bEnd.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Button bInfos = findViewById(R.id.infos_button);
        assert bInfos != null;
        bInfos.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                TextView showText = new TextView(ctx);
                showText.setText(R.string.exoweb);
                showText.setTextIsSelectable(true);
                showText.setAutoLinkMask(Linkify.ALL);
                Linkify.addLinks(showText, Linkify.WEB_URLS);
                AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                builder.setView(showText)
                        .setTitle(getString(R.string.about) + " kDI")
                        .setIcon(R.mipmap.ic_launcher)
                        .setCancelable(true)
                        .setNegativeButton(android.R.string.ok, null)
                        .show();
            }
        });
    }

    private void showActionsDialog() {
        final String sTvndk=getProp("ro.vndk.version");
        String[] actions = {
                getString(R.string.app_infos),
                getString(R.string.action_perm),
                "PRIVATE Ids Android/User/Serial/GSF" ,
                "proc/meminfo",
                "proc/cpuinfo",
                "?/etc/gps.conf",
                "getprop (aio)",
                "df (mounts)",
                "dumpsys media.extractor",
                "WebView provider(s)",
                "Treble linker namespace"+" ("+sTvndk+(getProp("ro.vndk.lite").equals("true")? "lite)":")"),
                "Matrix",
                "/vendor Manifest",
                "Location specs +MicroG",
                //"cat /proc/self/mounts"
//                "Refresh with SafetyNet",
                //"(Provision Widevine)"
        };
        new AlertDialog.Builder(this).setTitle("Select action").setItems(actions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        startActivity(new Intent(MainActivity.this, com.oF2pks.applicationsinfo.MainActivity.class));//.putExtra(EXTRA_PACKAGE_PERM,"Onboard.packages"));
                        break;
                    case 1:
                        startActivity(new Intent(MainActivity.this, com.oF2pks.chairlock.LaunchActivity.class));//.putExtra(EXTRA_PACKAGE_PERM,"Onboard.packages"));
                        break;
                    case 2:
//                        new CollectorTask().execute(true);
//                        break;
//                    case 3:
                        showIDs();
                        break;
                    case 3:
                        showZinfo("cat /proc/meminfo",true,true,false);
                        break;
                    case 4:
                        showZinfo("cat /proc/cpuinfo",true,false,true);
                        break;
                    case 5:
                        if (new File("/system/etc/gps.conf").exists())
                            showZinfo("cat /system/etc/gps.conf",true,true,true);
                        else showZinfo("cat /vendor/etc/gps.conf",true,true,true);
                        break;
                    case 6:
                        //showZinfo("getprop",true,false,false);
                        viewManifestIntent = new Intent(getBaseContext(), WebManifest.class);
                        viewManifestIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        viewManifestIntent.putExtra(WebManifest.EXTRA_MANIFEST_EXTRA, "getprop");
                        viewManifestIntent.putExtra(WebManifest.EXTRA_MANIFEST_PATH, manifestZinfo2("getprop",".",false, true));
                        getBaseContext().startActivity(viewManifestIntent);
                        break;
                    case 7:
                        showZinfo("df",true,true,false);
                        break;
                    case 8:
                        showZinfo("dumpsys media.extractor",true,false,true);
                        break;
                    case 9:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            Toast.makeText(getBaseContext(), WebManifest.webList(getBaseContext()), Toast.LENGTH_LONG).show();
                            String filePath = null;
                            try {
                                filePath = getPackageManager().getPackageInfo("android", 0).applicationInfo.sourceDir;
                                //applicationLabel = getPackageManager().getApplicationInfo("android", 0).loadLabel(getPackageManager()).toString();
                            } catch (PackageManager.NameNotFoundException e) {
                                break;
                            }
                            viewManifestIntent = new Intent(getBaseContext(), WebManifest.class);
                            viewManifestIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            viewManifestIntent.putExtra(WebManifest.EXTRA_MANIFEST_EXTRA, "WebViewProviders");
                            viewManifestIntent.putExtra(WebManifest.EXTRA_MANIFEST_PATH, AXMLPrinter.getManifestXMLFromAPK(filePath, "res/xml/config_webview_packages.xml"));
                            getBaseContext().startActivity(viewManifestIntent);

                            //showZinfo(AXMLPrinter.getManifestXMLFromAPK(filePath),false,false,false);
                        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            int id = getBaseContext().getResources().getIdentifier("android:string/config_webViewPackageName",null,null);
                            int id2 = getBaseContext().getResources().getIdentifier("android:string/config_alternateWebViewPackageName",null,null);
                            if (id!=0) {
                                String webV ="";
                                PackageManager pm = getPackageManager();
                                try {
                                    PackageInfo pi = pm.getPackageInfo((String) getBaseContext().getResources().getText(id), 0);
                                    webV = pi.versionName;
                                } catch (PackageManager.NameNotFoundException e) {
                                    webV = "Unknow";
                                }
                                showZinfo("\nUp: " + WebManifest.webV(getBaseContext(), true)
                                        + "\n" + WebManifest.webList(getBaseContext())
                                        + "\n<config_webViewPackageName>" + getBaseContext().getResources().getText(id)
                                        + (id2 == 0 ? "":"\n<config_alternateWebViewPackageName>" + getBaseContext().getResources().getText(id2))
                                        + "\n\nVersion: " + webV, false, false, false);
                            }
                        } else {
                            showZinfo(WebManifest.webV(getBaseContext(), false), false, false, false);
                        }
                        break;
                    case 10:
                        //https://source.android.com/devices/architecture/vndk#vndk-versioning
                        viewManifestIntent = new Intent(getBaseContext(), WebManifest.class);
                        viewManifestIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        viewManifestIntent.putExtra(WebManifest.EXTRA_MANIFEST_EXTRA, "ld.config");
                        if (getProp("ro.vndk.lite").equals("true")) viewManifestIntent.putExtra(WebManifest.EXTRA_MANIFEST_PATH,
                                manifestZinfo2("cat /system/etc/ld.config.vndk_lite.txt","]",false, false));
                        //showZinfo("cat /system/etc/ld.config.vndk_lite.txt",true,false,false);
                        else if (Utils.upR(sTvndk) || Build.VERSION.SDK_INT >= Build.VERSION_CODES.R )
                        {
                            viewManifestIntent.putExtra(WebManifest.EXTRA_MANIFEST_PATH,
                                    manifestZinfo2("cat /linkerconfig/ld.config.txt","]",false, false));
                            //showZinfo("cat /linkerconfig/ld.config.txt",true,false,false);
                        }
                        else viewManifestIntent.putExtra(WebManifest.EXTRA_MANIFEST_PATH,
                                    manifestZinfo2("cat /system/etc/ld.config"+(sTvndk.equals("") ?"": "."+sTvndk)+".txt","]",false, false));
                        //showZinfo("cat /system/etc/ld.config"+(sTvndk.equals("") ?"": "."+sTvndk)+".txt",true,false,false);
                        getBaseContext().startActivity(viewManifestIntent);
                        break;
                    case 11:
                        viewManifestIntent = new Intent(getBaseContext(), WebManifest.class);
                        viewManifestIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        if (Build.VERSION.SDK_INT <= 28) {
                            viewManifestIntent.putExtra(WebManifest.EXTRA_MANIFEST_PATH, "/system/compatibility_matrix.xml");
                            viewManifestIntent.putExtra(WebManifest.EXTRA_MANIFEST_EXTRA, "/system/compatibility_matrix.xml");
                        }
                        else {
                            viewManifestIntent.putExtra(WebManifest.EXTRA_MANIFEST_PATH,  copyFileToCache(getBaseContext(),new File("/system/etc/vintf/compatibility_matrix.device.xml"), "vintfCompatMATRIX.xml"));
                            viewManifestIntent.putExtra(WebManifest.EXTRA_MANIFEST_EXTRA, "/system/etc/vintf/compatibility_matrix.device.xml");
                        }
                        getBaseContext().startActivity(viewManifestIntent);
                        break;
                    case 12:
                        File vManifest = new File("/vendor/etc/vintf/manifest.xml");
                        String s ="vintf: ";
                        if (!vManifest.isFile()) {
                            vManifest = new File("/vendor/manifest.xml");
                            s="legacy: ";
                        }
                        if (vManifest.isFile()) {
                            viewManifestIntent = new Intent(getBaseContext(), WebManifest.class);
                            viewManifestIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            viewManifestIntent.putExtra(WebManifest.EXTRA_MANIFEST_EXTRA, s+vManifest);
                            viewManifestIntent.putExtra(WebManifest.EXTRA_MANIFEST_PATH, copyFileToCache(getBaseContext(), vManifest, "vendorMANIFEST.xml"));
                            getBaseContext().startActivity(viewManifestIntent);
                        }
                        break;
                    case 13:
                        showZinfo(ConfigXml.SearchLocationApp(getBaseContext()),false,true,false);
                        break;
                    case 14:
                        startActivity(new Intent(MainActivity.this, com.oF2pks.chairlock.LaunchActivity.class));//.putExtra(EXTRA_PACKAGE_PERM,"Onboard.packages"));
                        break;
                    /*case 6:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                            new AlertDialog.Builder(MainActivity.this).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    startProvision();
                                }
                            }).setNegativeButton("No", null).setMessage("Are you sure you want to attempt Widevine Provisioning?").show();
                        }
                        break;
                    */
                    case 15:
                        showZinfo("cat /proc/self/mounts",true,false,false);
                        break;
                }
            }
        }).show();
    }

    private void showExportDialog() {
        String[] actions;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && ((UiModeManager) getSystemService(UI_MODE_SERVICE)).getCurrentModeType() != Configuration.UI_MODE_TYPE_TELEVISION)
            actions = new String[]{getString(R.string.action_clipboard), getString(R.string.action_share),
                getString(R.string.action_save)};
        else
            actions = new String[]{getString(R.string.action_clipboard), getString(R.string.action_share),};

        new AlertDialog.Builder(this).setTitle("Select action").setItems(actions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String subject = Build.VERSION.RELEASE+Build.VERSION.INCREMENTAL;
                if (which == 0) {
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("kDI:" + Build.BRAND + "/" + Build.MODEL + "/" + Build.VERSION.SDK_INT + "/" + subject, report);
                    clipboard.setPrimaryClip(clip);
                } else if (which == 1) {
                    Intent shareIntent = intentWithText(subject, report);
                    startActivity(intentWithText(subject, report));
                } else if (which == 2 && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("application/json");
                    intent.putExtra(Intent.EXTRA_TITLE, subject + ".json");
                    startActivityForResult(intent, 88);
                }
            }
        }).show();
    }

    private void showZinfo(String s , boolean x , boolean b, boolean linky) {
        String t = "ROM Specifics & Implementations";
        TextView showText = new TextView(this);
        if (x) {
            t = s;
            showText.setText(getZinfo(s,"\n\u25A0",false));
        }
        else showText.setText(s);
        showText.setTextIsSelectable(true);
        if (b) {
            Typeface face = Typeface.createFromAsset(getAssets(), "fonts/RobotoMono-Bold.ttf");
            showText.setTypeface(face);
        }
        if (linky) {
            showText.setAutoLinkMask(Linkify.ALL);
            Linkify.addLinks(showText, Linkify.WEB_URLS);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this,android.R.style.Theme_Black);//Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
        builder.setView(showText)
                .setTitle(t)
                .setCancelable(true)
                .setNegativeButton(android.R.string.ok, null)
                .show();
    }

    /*@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    private void startProvision() {
        new ProvisionTask(this).execute();
    }

    private void provisionFailed(Exception e) {
    }

    private void provisionSuccessful() {
    }*/

    private Intent intentWithText(String subject, String report) {
        Intent sendIntent = new Intent();
        //E/ActivityTaskManager: Transaction too large, intent: Intent { act=android.intent.action.CHOOSER flg=0x800000 cmp=android/com.android.internal.app.ChooserActivity (has extras) }, extras size: 533064, icicle size: 0
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, report);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        sendIntent.setType("text/plain");
        return sendIntent;
    }

    private static class CollectorTask extends AsyncTask<Boolean, Void, String> {
        private WeakReference<MainActivity> mActivity = null;
        private WeakReference<Context> mContext = null;

        private CollectorTask (MainActivity pActivity, Context pContext) {
            link(pActivity, pContext);
        }
        private void link (MainActivity pActivity, Context pContext) {
            mActivity = new WeakReference<MainActivity>(pActivity);
            mContext = new WeakReference<>(pContext);
        }
        @Override
        protected String doInBackground(Boolean... params) {
            return Collector.getReport(mContext.get(), params[0]);
        }

        @Override
        protected void onPostExecute(String jsonString) {
            report = jsonString;
            mActivity.get().showReport(jsonString);
            mActivity.get().output = new File(mContext.get().getExternalFilesDir(null), (Build.VERSION.RELEASE+Build.VERSION.INCREMENTAL+".json").replaceAll(" ",""));

            try {
                FileWriter writer;
                writer = new FileWriter(mActivity.get().output);
                writer.write(report);
                writer.close();
                //if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.Q)
                Toast.makeText(mContext.get(), "Wrote report to " + mActivity.get().output, Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(mContext.get(), "Failed writing report: " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
        if (requestCode == 88) {
            if (resultCode == RESULT_OK) {
                if (resultData != null) {
                    final Uri uri = resultData.getData();
                    try {
                        InputStream input = new BufferedInputStream((InputStream)(new FileInputStream(output.getAbsoluteFile())));
                        ParcelFileDescriptor pfd = getBaseContext().getContentResolver().
                                openFileDescriptor(uri, "w");
                        FileOutputStream fos = new FileOutputStream(pfd.getFileDescriptor());
                        byte[] buffer = new byte[1024 * 4];
                        int n = 0;
                        while (-1 != (n = input.read(buffer))) {
                            fos.write(buffer, 0, n);
                        }

                        // Let the document provider know you're done by closing the stream.
                        fos.close();
                        pfd.close();
                        input.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // Perform operations on the document using its URI.
                }
            }
        }
    }

    /*private static class ProvisionTask extends AsyncTask<Context, Void, String> {

        private final Context context;

        public ProvisionTask(Context context) {
            this.context = context;
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
        @Override
        protected String doInBackground(Context... contexts) {
            try {
                provisionWidevine();
                return null;
            } catch (Exception e) {
                return e.toString();
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if (s == null) {
                Toast.makeText(context, "Provision Successful", Toast.LENGTH_LONG).show();
            } else {
                new AlertDialog.Builder(context).setTitle("Provision Failed").setMessage(s).show();
            }
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
        private void provisionWidevine() throws Exception {
            MediaDrm mediaDrm = new MediaDrm(Collector.WIDEVINE_UUID);
            MediaDrm.ProvisionRequest provisionRequest = mediaDrm.getProvisionRequest();
            String url = provisionRequest.getDefaultUrl() + "&signedRequest=" + new String(provisionRequest.getData());

            // send as empty post
            final HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            int responseCode = con.getResponseCode();
            if (responseCode >= 300) {
                throw new Exception("Bad response code " + responseCode);
            }
            BufferedInputStream bis = new BufferedInputStream(con.getInputStream());
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();

            int b;
            while ((b = bis.read()) >= 0) {
                baos.write(b);
            }
            bis.close();

            final byte[] response = baos.toByteArray();
            Log.d("RESULT", Base64.encodeToString(response, Base64.NO_WRAP));
            baos.close();

            mediaDrm.provideProvisionResponse(response);
            mediaDrm.release();
        }
    }*/
    private void showIDs() {
        TextView showText = new TextView(this);
        showText.setText(displayIDs());
        showText.setTextIsSelectable(true);
        Typeface face = Typeface.createFromAsset(getAssets(),"fonts/RobotoMono-Bold.ttf");
        showText.setTypeface(face);
        showText.setAutoLinkMask(Linkify.ALL);
        Linkify.addLinks(showText, Linkify.WEB_URLS);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(showText)
                .setTitle("(LongPress to select...) PRIVATE Ids")
                .setCancelable(true)
                .setNegativeButton(android.R.string.ok, null)
                .show();
    }

    private String displayIDs() {
        String marshmalow ="\nANDROIDid: "+ Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID)+"\n";
        if (Build.VERSION.SDK_INT < 28) marshmalow += "SERIALid:  "+Build.SERIAL+"\n";//getSerial()
        if (Build.VERSION.SDK_INT > 22) {
            marshmalow += "systemUSER: " + ((UserManager)getSystemService(USER_SERVICE)).isSystemUser()+"\n";
            marshmalow += "vbmetaDigest: " + getProp("ro.boot.vbmeta.digest")+"\n";
        }


        Cursor query = getContentResolver().query(Uri.parse("content://com.google.android.gsf.gservices"), null, null, new String[] { "android_id" }, null);
        if (query == null) {
            marshmalow+="GSFid:     unknow\n";
            return marshmalow;
        }
        if (!query.moveToFirst() || query.getColumnCount() < 2 || query.getString(1) == null) {
            marshmalow+="GSFid:     unknow\n";
            if (query.getString(1) == null) marshmalow+="No account, nu gsf...";
            query.close();
            return marshmalow;
        }
        final String toHexString = Long.toHexString(Long.parseLong(query.getString(1)));
        query.close();

        marshmalow+="\nGSFid:     "+ toHexString.toUpperCase().trim()+"\n\n";
        marshmalow+="REGISTER GSF https://www.google.com/android/uncertified\n\n";
        marshmalow+="More info https://www.xda-developers.com/how-to-fix-device-not-certified-by-google-error/";
        return marshmalow;
    }


    private static String copyFileToCache(@NonNull Context context, @NonNull File file, @NonNull String fName) {
        File output = new File(context.getExternalFilesDir(null), fName);
        String tmp = "" ;
        try {
            FileOutputStream outputStream = new FileOutputStream(output);
            BufferedInputStream bufferedInputStream = new BufferedInputStream((InputStream)(new FileInputStream(file.getAbsoluteFile())));
            byte[] buffer = new byte[1024 * 4];
            int n = 0;
            while (-1 != (n = bufferedInputStream.read(buffer))) {
                outputStream.write(buffer, 0, n);
                tmp += new String(buffer, "UTF-8").substring(0,n);
            }
            bufferedInputStream.close();
            outputStream.close();
        } catch (java.io.IOException e) {
            return e.getMessage();
        }
        return tmp;
    }
}
