package com.oF2pks.kalturadeviceinfos;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.SearchView;


import java.io.File;
import java.util.List;

public class WebManifest extends Activity implements SearchView.OnQueryTextListener {
    public static final String EXTRA_MANIFEST_PATH = "manifest_path";
    public static final String EXTRA_MANIFEST_EXTRA = "manifest_extra";
    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowCustomEnabled(true);

        SearchView searchView = new SearchView(actionBar.getThemedContext());
        searchView.setOnQueryTextListener(this);

        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        actionBar.setCustomView(searchView, layoutParams);

        mWebView = new WebView(this);
        setContentView(mWebView);
        String manifestPath = getIntent().getStringExtra(EXTRA_MANIFEST_PATH);

        WebSettings settings = mWebView.getSettings();
        settings.setBuiltInZoomControls(true);
        settings.setUseWideViewPort(true);
        //settings.setDefaultFontSize(12);
        //settings.setLoadWithOverviewMode(true);
        mWebView.setWebChromeClient(new WebChromeClient());
        if (manifestPath.startsWith("<?xml")) {
            actionBar.setTitle("Up: " + userWeb(mWebView.getSettings().getUserAgentString()));
            mWebView.loadData(manifestPath,"text/xml","UTF-8");
        }
        else {
            actionBar.setTitle(getIntent().getStringExtra(EXTRA_MANIFEST_EXTRA));
            if (manifestPath.startsWith("<")) {
                mWebView.loadData(manifestPath,"text/xml","UTF-8");
            } else mWebView.loadUrl(Uri.fromFile(new File(manifestPath)).toString());
        }
    }
    public static String userWeb(String s) {
        String tmp="Unknow";
        if (s.indexOf("Chrome") > 0) {
            tmp = s.substring(s.indexOf("Chrome"));
            if (s.indexOf(" ") > 0) {
                tmp = tmp.substring(0, tmp.indexOf(" "));
            }
        }
        return tmp;
    }
    public static String webV(Context ctx, boolean b) {
        WebView w = new WebView(ctx);
        w.setWebChromeClient(new WebChromeClient());
        if (b) return userWeb(w.getSettings().getUserAgentString());
        else return w.getSettings().getUserAgentString();
    }
    public static String webList(Context ctx) {
        String webList ="";
        List<ApplicationInfo> apps = ctx.getPackageManager().getInstalledApplications(0);
        for(ApplicationInfo app : apps) {
            if (app.packageName.contains(".webview")) {
                webList += app.packageName + ": "+ app.sourceDir + "\n";
            }
        }
        return webList;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mWebView.findNext(true);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mWebView.findAllAsync(newText);
        return true;
    }
}
