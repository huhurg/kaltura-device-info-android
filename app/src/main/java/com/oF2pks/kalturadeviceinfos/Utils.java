package com.oF2pks.kalturadeviceinfos;


import static com.oF2pks.applicationsinfo.utils.Utils.Html5Escapers;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.hardware.camera2.CameraCharacteristics;
import android.os.Build;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

class Utils {

    static boolean upR(String s) {
        if (s.length() == 0) return false;
        if (s.toUpperCase().compareTo("Q") > 0 ) return true;
        try {
            if (Integer.parseInt(s) >= 30) return true;
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    static String iCamList(CameraCharacteristics chars, CameraCharacteristics.Key<int[]> c, String[] s) {
        int [] z;
        try {
            z = chars.get(c);
        } catch (Exception e) {
            return e.getMessage();
        }
        String tmp ="";
        if (z != null) {
            for (int i:z) {
                if (i<s.length) tmp += "+ "+s[i];
                else tmp += "+ _#" + i;
            }
        } else return "Unknow";
        return tmp;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    static String fCamList(CameraCharacteristics chars, CameraCharacteristics.Key<float[]> c) {
        float [] z;
        try {
            z = chars.get(c);
        } catch (Exception e) {
            return e.getMessage();
        }
        String tmp ="";
        if (z != null) {
            for (float f:z) tmp += f+";";
        } else return "Unknow";
        return tmp;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    static String colorFilter(CameraCharacteristics chars) {
        Integer integer = chars.get(CameraCharacteristics.SENSOR_INFO_COLOR_FILTER_ARRANGEMENT);
        if (integer == null) return "";
        else if (integer == 0) {
            return"rggb";
        } else if (integer == 1) {
            return"grbg";
        } else if (integer == 2) {
            return"gbrg";
        } else if (integer == 3) {
            return"bggr";
        } else if (integer == 4) {
            return"rgb";
        } else if (integer == 5) {
            return"mono";
        } else if (integer == 6) {
            return"nir";
        }
        return "";
    }

    static String getProp(String s) {
        try {
            @SuppressLint("PrivateApi")
            Class<?> aClass = Class.forName("android.os.SystemProperties");
            Method method = aClass.getMethod("get", String.class);
            Object platform = method.invoke(null, s);

            return platform instanceof String ? (String) platform : "<" + platform + ">";

        } catch (Exception e) {
            return "null(<" + e + ">)";
        }
    }
    static JSONObject blindM(Object o) {
        JSONObject archT = new JSONObject();
        for ( Method m : o.getClass().getDeclaredMethods()) {
            try {
                if (m.getName().startsWith("get") ) archT.put(m.getName(), m.invoke(o));

            } catch (Exception e) {
                try {
                    archT.put(m.getName(), m.invoke(o,0));
                } catch (Exception ee) {
                }
            }

        }
        return archT;
    }
    static JSONObject semicolonJson(String s, String eq, String end) throws JSONException {
        JSONObject archT = new JSONObject();
        String tmp = s;
        int i,j= 0;
        while (tmp.contains(end)){
            i=tmp.indexOf(end);
            j=tmp.indexOf(eq);
            archT.put(tmp.substring(0,j),tmp.substring(j+1,i));
            tmp=tmp.substring(i+1);
        }
        j=tmp.indexOf(eq);
        archT.put(tmp.substring(0,j),tmp.substring(j+1));
        return archT;
    }
    static JSONObject semicolonSortedJson(String s, String eq, String end) throws JSONException {
        SortedSet set = new TreeSet();
        JSONObject archT = new JSONObject();
        String tmp = s;
        int i = 0;
        while (tmp.contains(end)){
            i=tmp.indexOf(end);
            set.add(tmp.substring(0,i));
            tmp=tmp.substring(i+1);
        }
        set.add(tmp);
        Iterator it = set.iterator();
        while (it.hasNext()) {
            tmp=it.next().toString();
            i=tmp.indexOf(eq);
            archT.put(tmp.substring(0,i),tmp.substring(i+1));
        }

        return archT;
    }

    static String getZinfo(String s, String sPlus, boolean bool) {
        try {
            Process p = Runtime.getRuntime().exec(s);
            InputStream is = null;
            //if (p.waitFor() == 0) {
            is = p.getInputStream();
            /*} else {
                is = p.getErrorStream();
            }*/
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String tmp;
            String tmp2 = "";

            if (bool) br.readLine();
            while ((tmp = br.readLine()) != null)
            {
                tmp2 +=sPlus+tmp;
            }
            is.close();
            br.close();
            p.destroy();
            if (tmp2.length() !=0) return tmp2;
            return "Unknow";
        } catch (Exception ex) {
            return "ERROR: " + ex.getMessage();
        }
    }

    static String manifestZinfo2(String s, String sPlus, boolean bool, boolean bTag) {
        try {
            Process p = Runtime.getRuntime().exec(s);
            InputStream is = null;
            //if (p.waitFor() == 0) {
            is = p.getInputStream();
            /*} else {
                is = p.getErrorStream();
            }*/
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String tmp;
            String tmp2 = "";
            String key = "";
            String key2 = "";
            int i;

            if (bool) br.readLine();
            while ((tmp = br.readLine()) != null)
            {
                tmp = Html5Escapers(tmp.replaceAll("\\[",""));
                if ((i = tmp.indexOf(sPlus)) >0 && !tmp.startsWith("#")) {
                    key = tmp.substring(0,i);
                    tmp = tmp.substring(i+1);
                    if (!key.equals(key2)) {
                        if (key2.length()>0) tmp2 += "\n</" + key2 + ">";
                        tmp2 += "\n<" + key +">";
                        key2 = key;
                    }
                } else key = "_";
                if (tmp.length()>0) tmp2 += "<" + (bTag?key+">":"_"+key2+"> ")
                        + tmp.replaceAll("]","")
                        .replace("#","\u25A0")//HTMLescaper
                        +" </" + (bTag?key:"_"+key2) + ">";


            }

            if ((i=s.lastIndexOf("/"))>0) s=s.substring(i+1);
            tmp2 = "<" + s +">\n" +tmp2 + "\n</" + key2 + ">\n</" + s +">\n";

            is.close();
            br.close();
            p.destroy();
            return tmp2;
        } catch (Exception ex) {
            return "ERROR: " + ex.getMessage();
        }
    }

}
